# Dotfiles: OSX Shortcuts

Save and restore your custom shortcuts settings found in System Preferences > Keyboard > Shortcuts > App Shortcuts.

![](screenshot.png)

## Usage

- `apps.txt` - list of applications to save/restore shortcuts settings from
- `./fetch.rb` - dumps shortcuts settings from all applications defined in `apps.txt` and saves in `configs/` directory
- `./apply.sh` - applies your settings from `configs/`
