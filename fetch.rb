#!/usr/bin/env ruby

require_relative 'common.rb'

File.read('apps.txt').each_line do |app|
  app.strip!
  next if app.empty?
  
  xml = Tempfile.open('osx-shortcuts') do |config|
    cmd = "defaults export '#{app}' - > '#{config.path}'"
    run cmd, debug: ENV.fetch('DEBUG') { false }
    config.read
  end

  doc = Nokogiri::XML xml
  elem = doc.at 'key:contains("NSUserKeyEquivalents")'
  
  unless elem
    puts "No custom shortcuts found for #{app}"
    next
  end

  xml = elem.next_element.to_xml
  File.write "configs/#{app}.xml", xml
end

