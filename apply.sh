#!/usr/bin/env bash

set -x

killall 'System Preferences'

ruby ./apply.rb

if [[ "$?" == '1' ]]; then
  sudo killall cfprefsd
fi
