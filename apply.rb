#!/usr/bin/env ruby

require_relative 'common.rb'

custom_app_added = false

def add_app_to_shortcuts_if_needed app
  if app == '.GlobalPreferences'
    app = 'NSGlobalDomain'
  end

  cmd = "defaults read com.apple.universalaccess com.apple.custommenu.apps"
  out = `#{cmd}`

  unless $?.success?
    `defaults write com.apple.universalaccess com.apple.custommenu.apps -array`
  end

  cmd = "defaults read com.apple.universalaccess com.apple.custommenu.apps"
  out = `#{cmd}`

  app_enabled = out.split("\n").each(&:strip).any? { |line| line.include? app }
  unless app_enabled
    cmd = "defaults write com.apple.universalaccess com.apple.custommenu.apps -array-add '#{app}' 2>&1"
    out = run cmd
    if out.include? 'Could not write domain com.apple.universalaccess; exiting'
      puts "ERROR: No permissions. Go to System Preferences > Security & Privacy > Privacy > Full Disk Access, and enable iTerm.app"
      exit 2
    end

    custom_app_added = true
  end
end

File.read('apps.txt').each_line do |app|
  app.strip!
  next if app.empty?

  add_app_to_shortcuts_if_needed app

  doc = Nokogiri::XML File.read "configs/#{app}.xml"
  doc.css('key').each do |key|
    item = key.text
    shortcut = key.next_element.text

    cmd = "defaults write '#{app}' NSUserKeyEquivalents -dict-add '#{item}' '#{shortcut}'"
    run cmd
  end
end

exit 1 if custom_app_added
