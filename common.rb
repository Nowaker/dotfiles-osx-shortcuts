require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'nokogiri', '~> 1.18.2'
end

require 'tempfile'
require 'nokogiri'

def run cmd, debug: true
  puts "+ #{cmd}" if debug
  out = `#{cmd}`.strip
  puts out unless out.empty?
  out
end
